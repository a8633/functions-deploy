image: registry.greenwavereality.com/greenwave/dind-kube:latest

stages:
  - analysis
  - secret
  - upload
  - build
  - release
  - cache_to_redis
  - trigger_kube_vedge
    #  - deploy

variables:
  DOCKER_VERISON: 19.03.12
  DOCKER_HOST: tcp://10.0.7.132:2375/
  DOCKER_REGISTRY: registry.greenwavereality.com
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: "/certs"
  IMAGE_NAME: axon-vedge/pulsar-2.7.1-axon-functions
  APP_NAME: pulsar-functions #[X] TODO:specify APP_NAME param in the document
  SHORT_SHA: $CI_COMMIT_SHORT_SHA #[X] TODO:specify SHORT_SHA param in the document
  VERSION: $CI_COMMIT_REF_NAME #[X]TODO:specify VERSION param in the document
  TRIGERRING_COMMIT_BRANCH: $CI_COMMIT_BRANCH

  CONTAINER_NS_NAME: $CI_REGISTRY/$CI_IMAGE_NAME
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY/$IMAGE_NAME:latest
  CONTAINER_RELEASE_TAG: $CI_REGISTRY/$IMAGE_NAME:$CI_COMMIT_REF_NAME
  CONTAINER_TEST_IMAGE: $CI_REGISTRY/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA

services:
  - docker:${DOCKER_VERSION}-dind

before_script:
  - docker info || true
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY || true

# This will check our code
# sonarqube:
#   stage: analysis
#   image:
#     name: registry.greenwavereality.com/axon/sonar-scanner:latest
#     entrypoint: [""]
#   variables:
#     SONAR_URL: http://sonarqube.axon-networks.com
#     SONAR_ANALYSIS_MODE: publish
#     SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
#     GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
#   cache:
#     key: "${CI_JOB_NAME}"
#     paths:
#       - .sonar/cache
#   script:
#     - gitlab-sonar-scanner

#This will check our code in Blackduck-Hub
blackduck:
  stage: analysis
  image: registry.greenwavereality.com/greenwave/blackduck-scanner-maven:1.2
  script:
    - >
      mvn install -pl axon_pulsar_functions/axon_edge_protocol,axon_pulsar_functions/axon_edge_utils
  after_script:
    - >
      bash <(curl -s https://detect.synopsys.com/detect.sh)
      --blackduck.url="${HUB_URL}"
      --blackduck.username="${HUB_USER}"
      --blackduck.password="${HUB_PASSWORD}"
      --detect.project.version.name="${CI_PROJECT_NAME}"
      --detect.project.version.notes="${CI_COMMIT_REF_NAME}"
      --detect.project.name=GWS
      --detect.project.version.phase=DEVELOPMENT
      --detect.blackduck.signature.scanner.memory=12288
      --detect.excluded.detector.types=yarn,npm,git
      --detect.sbt.report.depth=20
      --detect.detector.search.depth=20
      --detect.blackduck.signature.scanner.exclusion.pattern.search.depth=20
      --blackduck.trust.cert=true
      --detect.policy.check=true

secret:
  stage: secret
  image: registry.greenwavereality.com/greenwave/vault
  secrets:
    SIGN_KEY:
      vault: vEdge/codesign.key/cert@secret
  script:
    # - *p_script
    - echo $CI_PROJECT_NAME
    - echo $CI_JOB_JWT
    # Vault's address can be provided here or as CI variable
    - export VAULT_ADDR=http://gitlab.greenwavereality.com:8200
    # Authenticate and get token. Token expiry time and other properties can be configured
    # when configuring JWT Auth - https://www.vaultproject.io/api/auth/jwt#parameters-1
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=vedge-monkey jwt=$CI_JOB_JWT)"
    # Now use the VAULT_TOKEN to read the secret and store it in an environment variable
    - export CERT="$(vault kv get -field=cert secret/vEdge/codesign.key)"
    # Use the secret
    - echo $CERT > sign.key
    # - vault read -field=mirrorkey secret/mirror-web | awk '{$1=$1;print}' > mirrorssh.pem
    - vault kv get -field=mirrorkey secret/mirror-web  > mirrorssh.pem
    - |
      case $N_SPACE in
        *staging*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-staging > kube.config;;
        *demo*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-poc > kube.config;;
        *senia*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-demo > kube.config;;
        *grain*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-demo > kube.config;;
        *brsk*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-demo > kube.config;;
        *openinfra*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-north-2 > kube.config;;
        *trial*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-north-2 > kube.config;;
        *fastspeed*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-north-2 > kube.config;;
        *swishfibre*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-cloud-north-2 > kube.config;;
        *pulsar*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-demo > kube.config;;
        *arman*) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey-demo > kube.config;;
        *) vault kv get -field=kubeconfig vedge/k8configs/vedge-monkey > kube.config;;
      esac
  artifacts:
    expire_in: 10 mins
    paths:
      - ./sign.key
      - ./kube.config
      - ./mirrorssh.pem

compile:
  stage: upload
  script:
    # get id of container
    - cat ./mirrorssh.pem
    - export CONTAINER_ID=$(docker ps -q -f "label=com.gitlab.gitlab-runner.job.id=$CI_JOB_ID" -f "label=com.gitlab.gitlab-runner.type=build")
    # get mount name
    - export MOUNT_NAME=$(docker inspect $CONTAINER_ID -f "{{ range .Mounts }}{{ if eq .Destination \"/builds/${CI_PROJECT_NAMESPACE}\" }}{{ .Source }}{{end}}{{end}}" | cut -d "/" -f 6)
    # run container
    #- docker run --volumes-from $CONTAINER_ID -v $MOUNT_NAME:/builds -w /builds/$CI_PROJECT_NAME --entrypoint=/bin/sh busybox -c "ls -la /builds"
    - VERSION=$VERSION make version
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make builder
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make build
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make package
    # - VERSION=${SIMPLIFIED_VERSION} SHARED_PATH=${SHARED_PATH} MIRROR_USER=monkeyci make push
    # - echo API_VERSION="${API_VERSION}"
  artifacts:
    expire_in: 10 mins
    paths:
      - ./release
  only:
    - master

compile-and-upload:
  # This will upload the build binaries and config files to mirror web server
  # everytime a Git tag is created
  stage: upload
  script:
    # get id of container
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    # Run ssh-agent (inside the build environment)
    - eval $(ssh-agent -s)
    # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    - chmod 600 ./mirrorssh.pem
    # - cat ./mirrorssh.pem  > ./mirrorssh.pem
    # - cat mirrorssh.pem
    # - ssh-add -K ./mirrorssh.pem
    # - cat ./mirrorssh.pem | tr -d '\r' | ssh-add -
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - '[[ -d ~/.ssh ]] && echo ".ssh exists" || mkdir -p ~/.ssh'
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - cat ./mirrorssh.pem
    - export CONTAINER_ID=$(docker ps -q -f "label=com.gitlab.gitlab-runner.job.id=$CI_JOB_ID" -f "label=com.gitlab.gitlab-runner.type=build")
    # get mount name
    - export MOUNT_NAME=$(docker inspect $CONTAINER_ID -f "{{ range .Mounts }}{{ if eq .Destination \"/builds/${CI_PROJECT_NAMESPACE}\" }}{{ .Source }}{{end}}{{end}}" | cut -d "/" -f 6)
    # run container
    - VERSION=$VERSION make version
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make builder
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make build
    - VERSION=${VERSION} SHARED_PATH=${SHARED_PATH} make package
    - MIRROR_USER=monkeyci VERSION=${VERSION} make push
  artifacts:
    expire_in: 10 mins
    paths:
      - ./release
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(|(-(staging|dev|demo|prod|pulsar|grain|brsk|openinfra|trial|fastspeed|swishfibre|senia|arman|pulsar|grain|brsk|openinfra|trial|fastspeed|swishfibre|senia|arman)))$/'
      when: on_success
    - when: never

build:
  # image: registry.greenwavereality.com/axon-vedge/pulsar-functions-builder:latest
  stage: build
  script:
    - ls release
    - docker pull $CONTAINER_RELEASE_IMAGE || true
    # - docker build --build-arg VERSION="${VERSION}" --pull --cache-from $CONTAINER_RELEASE_IMAGE -t $CONTAINER_TEST_IMAGE -t $CONTAINER_RELEASE_IMAGE .
    - docker build --build-arg VERSION="${VERSION}" -t $CONTAINER_TEST_IMAGE -t $CONTAINER_RELEASE_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE

release-image:
  stage: release
  script:
    - ls release
    - docker pull $CONTAINER_TEST_IMAGE
    - echo $CONTAINER_TEST_IMAGE
    - echo "GLOBAL INHERITANCE TRY"
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - master

release-tag:
  # Finally, the goal here is for Docker to tag any Git tag gitLab will start a new
  # pipeline everytime a Git tag is created
  stage: release
  script:
    - echo API_VERSION="${API_VERSION}"
    - docker pull $CONTAINER_TEST_IMAGE
    - echo "API_VERSION=${CI_COMMIT_REF_NAME}" >> tag.env
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_TAG
    - docker push $CONTAINER_RELEASE_TAG
    - docker push $CONTAINER_RELEASE_IMAGE
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(|(-(staging|dev|demo|prod|pulsar|grain|brsk|openinfra|trial|fastspeed|swishfibre|senia|arman)))$/'
      when: on_success
    - when: never

cache_to_redis:
  stage: cache_to_redis
  before_script:
    - echo "cancel before script...."
  image:
    name: registry.greenwavereality.com/devops/deployment-worker
    entrypoint: [""]
  variables:
    BRANCH_OF_TAGGED_COMMIT: ${BRANCH_OF_TAGGED_COMMIT//\"/}
  script:
    - |
      export TEMP_VAL=$(case $VERSION in
        *grain*) echo "grain";;
        *brsk*) echo "brsk";;
        *openinfra*) echo "openinfra";;
        *trial*) echo "trial";;
        *fastspeed*) echo "fastspeed";;
        *swishfibre*) echo "swishfibre";;
        *senia*) echo "senia";;
        *pulsar*) echo "pulsar";;
        *arman*) echo "arman";;
        *staging*) echo "staging";;
        *demo*) echo "demo";;
        *prod*) echo "prod";;
        *dev*) echo "dev";;
        *) echo "dev";;
      esac);
    - echo "BRANCH_OF_TAGGED_COMMIT=\"$TEMP_VAL\"" > global.env
    - cat global.env
    - export
    - echo "this is tagged commit"
    - echo $BRANCH_OF_TAGGED_COMMIT
    - echo $VERSION
    - REDISCLI_AUTH=$CI_REDIS_CACHE redis-cli -h gitlab.greenwavereality.com set $VERSION $TEMP_VAL
    - REDISCLI_AUTH=$CI_REDIS_CACHE redis-cli -h gitlab.greenwavereality.com set $SHORT_SHA $TEMP_VAL
  artifacts:
    expire_in: 10 mins
    reports:
      dotenv: global.env
  needs: ["compile-and-upload"]
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(|(-(staging|dev|demo|prod|pulsar|grain|brsk|openinfra|trial|fastspeed|swishfibre|senia|arman)))$/'
      when: on_success
    - when: never

trigger:
  stage: trigger_kube_vedge
  trigger: axon-orchestrator/kube_vedge
  needs: ["cache_to_redis"]
  rules:
    - if: '$CI_COMMIT_REF_NAME =~ /^v[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(|(-(staging|dev|demo|prod|pulsar|grain|brsk|openinfra|trial|fastspeed|swishfibre|senia|arman)))$/'
      when: on_success
    - when: never
