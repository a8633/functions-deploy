
/**
 * 
 */
package com.axon_networks.pulsar;

import java.sql.Timestamp;

import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.apache.pulsar.common.schema.KeyValue;
import org.apache.pulsar.functions.api.Context;
import org.apache.pulsar.functions.api.Function;

/**
 * @author armangurkan
 *
 */
public class secondfunction implements Function<String, Void> {
  @Override
  public Void process(String input, Context context) {
    String publishTopic = (String) context.getOutputTopic();
    byte[] redisKey = ("status_" + new Timestamp(System.currentTimeMillis()).toString()).getBytes();
    byte[] output = String.format("%s And I am doing great!!!", input).getBytes();
    KeyValue<byte[], byte[]> msg = new KeyValue<byte[], byte[]>(redisKey, output);
    try {
      context.newOutputMessage(publishTopic, Schema.KV_BYTES()).value(msg).sendAsync();
    } catch (PulsarClientException e) {
      context.getLogger().error(e.toString());
    }

    return null;
  }
}
