
/**
 * 
 */
package com.axon_networks.pulsar;

import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.apache.pulsar.functions.api.Context;
import org.apache.pulsar.functions.api.Function;

/**
 * @author armangurkan
 *
 */
public class firstfunc implements Function<String, Void> {
  @Override
  public Void process(String input, Context context) {
    String publishTopic = (String) context.getOutputTopic();
    String output = String.format("My name is %s.", input);
    try {
      context.newOutputMessage(publishTopic, Schema.STRING).value(output).sendAsync();
    } catch (PulsarClientException e) {
      context.getLogger().error(e.toString());
    }

    return null;
  }
}
